; Include the definition for how to build Drupal core directly.
api = 2
core = 7.x

projects[drupal][version] = 7.54

; Patches added:
projects[drupal][patch][] = "https://www.drupal.org/files/issues/node_access_for_left_joins_d7.1349080-332.patch"
